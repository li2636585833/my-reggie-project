package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Employee;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Transactional
public interface EmployeeService extends IService<Employee> {
    Result<Employee> login(HttpServletRequest request, Employee employee);

    Result<String> logout(HttpServletRequest request);

    Result<String> addEmp(HttpServletRequest request, Employee employee);

    Result<Page<Employee>> getEmpListByPage(int page, int pageSize, String name);

    Result<String> updateEmp(HttpServletRequest request, Employee employee);

    Result<Employee> getEmpById(Long id);

    Result<String> deleteEmpById(Long id);
}
