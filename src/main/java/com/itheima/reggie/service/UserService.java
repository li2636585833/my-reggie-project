package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.UserDto;
import com.itheima.reggie.entity.User;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Transactional
public interface UserService extends IService<User> {
    Result<String> sendMsgToUser(HttpServletRequest request, User user);

    Result<User> userLogin(HttpServletRequest request, UserDto userDto);
}
