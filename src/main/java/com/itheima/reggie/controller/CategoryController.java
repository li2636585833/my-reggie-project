package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 菜品列表分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public Result<Page<Category>> getCategoryListByPage(int page, int pageSize) {
        return categoryService.getCategoryListByPage(page, pageSize);
    }

    /**
     * 新增分类
     *
     * @param category
     * @return
     */
    @PostMapping
    public Result<String> addCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }

    /**
     * 删除分类
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    public Result<String> deleteCategoryById(@RequestParam List<Long> ids) {
        log.info("id:{}", ids);
        return categoryService.deleteCategoryById(ids);
    }

    /**
     * 修改分类
     *
     * @param category
     * @return
     */
    @PutMapping
    public Result<String> updateCategory(@RequestBody Category category) {
        return categoryService.updateCategory(category);
    }

    /**
     * 根据类型查询分类列表信息
     *
     * @param category
     * @return
     */
    @GetMapping("/list")
    public Result<List<Category>> getCategoryListByType(Category category) {
        return categoryService.getCategoryListByType(category);
    }
}
