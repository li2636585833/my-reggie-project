package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.mapper.AddressBookMapper;
import com.itheima.reggie.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {

    @Autowired
    private AddressBookMapper addressBookMapper;

    /**
     * 新增收货地址
     *
     * @param addressBook
     * @return
     */
    @Override
    public Result<AddressBook> addAddressBook(AddressBook addressBook) {
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBookMapper.insert(addressBook);
        return Result.success(addressBook);
    }

    /**
     * 设置默认地址
     *
     * @param addressBook
     * @return
     */
    @Override
    public Result<AddressBook> setDefault(AddressBook addressBook) {
        Long id = BaseContext.getCurrentId();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, id);
        List<AddressBook> addressBookList = addressBookMapper.selectList(queryWrapper);
        for (AddressBook item : addressBookList) {
            item.setIsDefault(0);
            addressBookMapper.updateById(item);
        }

        addressBook.setIsDefault(1);
        addressBookMapper.updateById(addressBook);

        return Result.success(addressBook);
    }

    /**
     * 根据id查询地址
     *
     * @param id
     * @return
     */
    @Override
    public Result<AddressBook> getAddressBookById(Long id) {
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getId, id);
        AddressBook addressBook = addressBookMapper.selectOne(queryWrapper);
        if (addressBook != null) {
            return Result.success(addressBook);
        }
        return Result.error("该地址不存在！");
    }

    /**
     * 获取默认地址
     *
     * @return
     */
    @Override
    public Result<AddressBook> getDefaultAddressBook() {
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId());
        queryWrapper.eq(AddressBook::getIsDefault, 1);
        AddressBook addressBook = addressBookMapper.selectOne(queryWrapper);
        if (addressBook != null) {
            return Result.success(addressBook);
        }
        return Result.error("默认地址不存在！");
    }

    /**
     * 获取当前登录用户的全部地址
     *
     * @param addressBook
     * @return
     */
    @Override
    public Result<List<AddressBook>> getAddressBookList(AddressBook addressBook) {
        /*Long id = BaseContext.getCurrentId();
        addressBook.setUserId(id);
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(addressBook.getUserId() != null, AddressBook::getUserId, addressBook.getUserId());
        queryWrapper.orderByDesc(AddressBook::getUpdateTime);
        List<AddressBook> addressBookList = addressBookMapper.selectList(queryWrapper);
        return Result.success(addressBookList);*/
        Long userId = BaseContext.getCurrentId();
        LambdaQueryWrapper<AddressBook> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AddressBook::getUserId, userId);
        lambdaQueryWrapper.orderByDesc(AddressBook::getUpdateTime);
        List<AddressBook> addressBooks = addressBookMapper.selectList(lambdaQueryWrapper);
        return Result.success(addressBooks);
    }

    /**
     * 修改地址
     *
     * @param addressBook
     * @return
     */
    @Override
    public Result<String> updateAddressBook(AddressBook addressBook) {
        addressBookMapper.updateById(addressBook);
        return Result.success("修改成功！");
    }

    /**
     * 删除地址
     *
     * @param id
     * @return
     */
    @Override
    public Result<String> deleteAddressBookById(Long id) {
        addressBookMapper.deleteById(id);
        return Result.success("删除成功！");
    }

}
