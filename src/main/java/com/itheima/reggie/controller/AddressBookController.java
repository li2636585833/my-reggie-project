package com.itheima.reggie.controller;

import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    /**
     * 新增收货地址
     *
     * @param addressBook
     * @return
     */
    @PostMapping
    public Result<AddressBook> addAddressBook(@RequestBody AddressBook addressBook) {
        log.info("移动端传来的地址为：{}", addressBook);
        return addressBookService.addAddressBook(addressBook);
    }

    /**
     * 设置默认地址
     *
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    public Result<AddressBook> setDefault(@RequestBody AddressBook addressBook) {
        log.info("前台设置的默认地址为：{}", addressBook);
        return addressBookService.setDefault(addressBook);
    }

    /**
     * 根据id查询地址
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<AddressBook> getAddressBookById(@PathVariable Long id) {
        log.info("前台传来要查询的地址id为：{}", id);
        return addressBookService.getAddressBookById(id);
    }

    /**
     * 获取默认地址
     *
     * @return
     */
    @GetMapping("/default")
    public Result<AddressBook> getDefaultAddressBook() {
        return addressBookService.getDefaultAddressBook();
    }

    /**
     * 获取当前登录用户的全部地址
     *
     * @param addressBook
     * @return
     */
    @GetMapping("/list")
    public Result<List<AddressBook>> getAddressBookList(AddressBook addressBook) {
        return addressBookService.getAddressBookList(addressBook);
    }

    /**
     * 修改地址
     *
     * @param addressBook
     * @return
     */
    @PutMapping
    public Result<String> updateAddressBook(@RequestBody AddressBook addressBook) {
        return addressBookService.updateAddressBook(addressBook);
    }

    /**
     * 删除地址
     *
     * @param id
     * @return
     */
    @DeleteMapping
    public Result<String> deleteAddressBook(Long id) {
        return addressBookService.deleteAddressBookById(id);
    }
}