package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.mapper.OrderMapper;
import com.itheima.reggie.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Orders> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private AddressBookService addressBookService;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private DishDtoService dishDtoService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private SetMealService setMealService;

    /**
     * 订单明细列表分页查询
     *
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public Result<Page<OrderDto>> getOrderListByPage(int page, int pageSize, String number, LocalDateTime beginTime, LocalDateTime endTime) {

        Page<Orders> orderPage = new Page<>(page, pageSize);
        Page<OrderDto> orderDtoPage = new Page<>();
        LambdaQueryWrapper<Orders> orderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        orderLambdaQueryWrapper.eq(number != null, Orders::getNumber, number);
        orderLambdaQueryWrapper.gt(beginTime != null, Orders::getOrderTime, beginTime);
        orderLambdaQueryWrapper.lt(endTime != null, Orders::getOrderTime, endTime);
        orderLambdaQueryWrapper.orderByDesc(Orders::getCheckoutTime);
        orderMapper.selectPage(orderPage, orderLambdaQueryWrapper);
        BeanUtils.copyProperties(orderPage, orderDtoPage, "records");
        List<Orders> ordersList = orderPage.getRecords();
        List<OrderDto> orderDtoList = ordersList.stream().map((order) -> {
            OrderDto orderDto = new OrderDto();
            BeanUtils.copyProperties(order, orderDto);
            User user = userService.getById(order.getUserId());
            if (user != null) {
                orderDto.setUserName(user.getName());
            }
            return orderDto;
        }).collect(Collectors.toList());
        orderDtoPage.setRecords(orderDtoList);
        return Result.success(orderDtoPage);
    }

    /**
     * 立即支付
     *
     * @param orders
     * @return
     */
    @Override
    public Result<String> orderPay(Orders orders) {
        //获取当前登录用户id
        Long userId = BaseContext.getCurrentId();

        //根据用户id查询购物车信息
        LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
        shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, userId);
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(shoppingCartLambdaQueryWrapper);
        if (shoppingCartList == null || shoppingCartList.size() == 0) {
            throw new CustomException("购物车为空！");
        }

        //根据地址id查询地址信息
        Long addressBookId = orders.getAddressBookId();
        AddressBook addressBook = addressBookService.getById(addressBookId);
        if (addressBook == null) {
            throw new CustomException("收货地址为空！");
        }

        //统计总金额
        AtomicInteger amount = new AtomicInteger(0);

        //订单id
        long orderId = IdWorker.getId();

        //将购物车中信息设置到订单详情中
        List<OrderDetail> orderDetails = shoppingCartList.stream().map((item) -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(orderId);
            orderDetail.setNumber(item.getNumber());
            orderDetail.setDishFlavor(item.getDishFlavor());
            orderDetail.setDishId(item.getDishId());
            orderDetail.setSetmealId(item.getSetmealId());
            orderDetail.setName(item.getName());
            orderDetail.setImage(item.getImage());
            orderDetail.setAmount(item.getAmount());
            amount.addAndGet(item.getAmount().multiply(new BigDecimal(item.getNumber())).intValue());
            return orderDetail;
        }).collect(Collectors.toList());

        //获取当前登录用户
        User user = userService.getById(userId);

        //设置订单表数据
        //orders.setId(orderId);
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setStatus(2);
        orders.setAmount(new BigDecimal(amount.get()));//总金额
        orders.setUserId(userId);
        orders.setNumber(String.valueOf(orderId));
        orders.setUserName(user.getName());
        orders.setConsignee(addressBook.getConsignee());
        orders.setPhone(addressBook.getPhone());
        orders.setAddress((addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
                + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
                + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
                + (addressBook.getDetail() == null ? "" : addressBook.getDetail()));

        //向订单表插入数据，一条数据
        this.save(orders);

        //向订单明细表插入数据，多条数据
        orderDetailService.saveBatch(orderDetails);

        //清空购物车数据
        shoppingCartService.remove(shoppingCartLambdaQueryWrapper);

        return Result.success("支付成功！");
    }

    /**
     * 移动端用户订单查看
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Result<Page<OrderDto>> userPage(int page, int pageSize) {
        Page<Orders> orderPage = new Page<>(page, pageSize);
        Page<OrderDto> orderDtoPage = new Page<>();
        LambdaQueryWrapper<Orders> orderLambdaQueryWrapper = new LambdaQueryWrapper<>();
        orderLambdaQueryWrapper.orderByDesc(Orders::getCheckoutTime);
        orderMapper.selectPage(orderPage, orderLambdaQueryWrapper);
        BeanUtils.copyProperties(orderPage, orderDtoPage, "records");
        List<Orders> ordersList = orderPage.getRecords();
        List<OrderDto> orderDtoList = ordersList.stream().map((order) -> {
            OrderDto orderDto = new OrderDto();
            BeanUtils.copyProperties(order, orderDto);
            User user = userService.getById(order.getUserId());
            if (user != null) {
                orderDto.setUserName(user.getName());
            }
            LambdaQueryWrapper<OrderDetail> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(OrderDetail::getOrderId, order.getNumber());
            List<OrderDetail> orderDetailList = orderDetailService.list(queryWrapper);
            orderDto.setOrderDetails(orderDetailList);
            return orderDto;
        }).collect(Collectors.toList());
        orderDtoPage.setRecords(orderDtoList);
        return Result.success(orderDtoPage);
    }

    /**
     * 修改订单状态为派送完成
     *
     * @param orders
     * @return
     */
    @Override
    public Result<String> updateOrderStatusTo3(Orders orders) {
        orderMapper.updateById(orders);
        return Result.success("修改成功！");
    }

    /**
     * 再来一单
     *
     * @param orders
     * @return
     */
    @Override
    public Result<String> orderAgain(Orders orders) {
        Orders orders1 = orderMapper.selectById(orders.getId());
        Long userId = orders1.getUserId();
        LambdaQueryWrapper<ShoppingCart> shoppingCartLambdaQueryWrapper = new LambdaQueryWrapper<>();
        shoppingCartLambdaQueryWrapper.eq(ShoppingCart::getUserId, userId);
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(shoppingCartLambdaQueryWrapper);
        if (shoppingCartList != null && shoppingCartList.size() > 0) {
            throw new CustomException("购物车不为空，请先付款！");
        }
        LambdaQueryWrapper<OrderDetail> orderDetailLambdaQueryWrapper = new LambdaQueryWrapper<>();
        orderDetailLambdaQueryWrapper.eq(OrderDetail::getOrderId, orders1.getNumber());
        List<OrderDetail> orderDetailList = orderDetailService.list(orderDetailLambdaQueryWrapper);
        for (OrderDetail orderDetail : orderDetailList) {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setName(orderDetail.getName());
            shoppingCart.setImage(orderDetail.getImage());
            shoppingCart.setUserId(userId);
            if (orderDetail.getDishId() != null) {
                shoppingCart.setDishId(orderDetail.getDishId());
                shoppingCart.setDishFlavor(orderDetail.getDishFlavor());
            } else {
                shoppingCart.setSetmealId(orderDetail.getSetmealId());
            }
            shoppingCart.setNumber(orderDetail.getNumber());
            shoppingCart.setAmount(orderDetail.getAmount());
            shoppingCartService.save(shoppingCart);
        }
        return Result.success("再来一单！");
    }
}
