package com.itheima.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebFilter("/*")
public class LoginFilter implements Filter {

    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        log.info("拦截到请求:{}", request.getRequestURI());
        //1、获取本次请求的URI
        String uri = request.getRequestURI();
        //2、判断本次请求是否需要处理
        String[] urls = {"/employee/login", "/employee/logout", "/backend/**", "/front/**",
                "/user/sendMsg", "/user/login"};
        //3、如果不需要处理，则直接放行
        boolean check = check(urls, uri);
        if (check) {
            filterChain.doFilter(request, response);
            return;
        }
        //4.1 判断后台登录状态，如果已登录，则直接放行
        Employee emp = (Employee) request.getSession().getAttribute("emp");
        if (emp != null) {
            BaseContext.setCurrentId(emp.getId());
            filterChain.doFilter(request, response);
            return;
        }

        //4.2 判断移动端登录状态，如果已登录，则直接放行
        User user = (User) request.getSession().getAttribute("user");
        if (user != null) {
            BaseContext.setCurrentId(user.getId());
            filterChain.doFilter(request, response);
            return;
        }

        //5、如果未登录则返回未登录结果
        response.getWriter().write(JSON.toJSONString(Result.error("NOTLOGIN")));
    }

    /**
     * 判断用户请求的地址是否需要拦截
     *
     * @param urls
     * @param uri
     * @return
     */
    public boolean check(String[] urls, String uri) {
        for (String url : urls) {
            if (PATH_MATCHER.match(url, uri)) {
                return true;
            }
        }
        return false;
    }
}
