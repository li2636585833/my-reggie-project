package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetMealDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.SetMealMapper;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetMealDishService;
import com.itheima.reggie.service.SetMealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetMealServiceImpl extends ServiceImpl<SetMealMapper, Setmeal> implements SetMealService {

    @Autowired
    private SetMealMapper setMealMapper;

    @Autowired
    private SetMealDishService setMealDishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishService dishService;

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @CacheEvict(value = "SetMealCache", allEntries = true)
    @CachePut(value = "SetMealCache", key = "#setmealDto.id")
    @Override
    public Result<String> addSetMeal(SetMealDto setmealDto) {
        //1.将继承属性插入setmeal表
        this.save(setmealDto);
        //2.将独有属性插入setmeal_dish表
        //2.1 先获取刚刚插入setmeal表的数据的id
        Long setMealId = setmealDto.getId();
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //2.2 给每一个setmealDish设置setMealId
        setmealDishes = setmealDishes.stream().map((item) -> {
            item.setSetmealId(setMealId);
            return item;
        }).collect(Collectors.toList());
        //2.3 插入setmeal_dish表
        setMealDishService.saveBatch(setmealDishes);

        return Result.success("新增成功！");
    }

    /**
     * 套餐列表分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Result<Page<SetMealDto>> getSetMealDtoList(int page, int pageSize, String name) {
        Page<Setmeal> setMealPage = new Page<>(page, pageSize);
        Page<SetMealDto> setMealDtoPage = new Page<>();
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name != null, Setmeal::getName, name);
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        setMealMapper.selectPage(setMealPage, queryWrapper);
        BeanUtils.copyProperties(setMealPage, setMealDtoPage, "records");
        List<Setmeal> setMealList = setMealPage.getRecords();
        List<SetMealDto> setMealDtoList = setMealList.stream().map((item) -> {
            SetMealDto setMealDto = new SetMealDto();
            BeanUtils.copyProperties(item, setMealDto);
            Category category = categoryService.getById(item.getCategoryId());
            if (category != null) {
                String categoryName = category.getName();
                setMealDto.setCategoryName(categoryName);
            }
            return setMealDto;
        }).collect(Collectors.toList());
        setMealDtoPage.setRecords(setMealDtoList);
        return Result.success(setMealDtoPage);
    }

    /**
     * 根据id查询套餐详情
     *
     * @param id
     * @return
     */
    @Override
    public Result<SetMealDto> getSetMealById(Long id) {
        SetMealDto setMealDto = new SetMealDto();
        Setmeal setmeal = setMealMapper.selectById(id);
        BeanUtils.copyProperties(setmeal, setMealDto);
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> setmealDishList = setMealDishService.list(queryWrapper);
        setMealDto.setSetmealDishes(setmealDishList);
        return Result.success(setMealDto);
    }

    /**
     * 修改套餐
     *
     * @param setMealDto
     * @return
     */
    @Override
    public Result<String> updateSetMeal(SetMealDto setMealDto) {
        this.saveOrUpdate(setMealDto);

        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, setMealDto.getId());
        setMealDishService.remove(queryWrapper);

        List<SetmealDish> setmealDishList = setMealDto.getSetmealDishes();
        setmealDishList = setmealDishList.stream().map((item) -> {
            item.setSetmealId(setMealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setMealDishService.saveBatch(setmealDishList);
        return Result.success("修改成功！");
    }

    /**
     * 删除套餐
     *
     * @param ids
     * @return
     */
    @CacheEvict(value = "SetMealCache", allEntries = true)
    @Override
    public Result<String> deleteSetMeal(List<Long> ids) {

        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.in(Setmeal::getId, ids);
        setmealLambdaQueryWrapper.eq(Setmeal::getStatus, 1);
        int count = this.count(setmealLambdaQueryWrapper);
        if (count > 0) {
            throw new CustomException("套餐正在售卖，不能删除！");
        }

        this.removeByIds(ids);

        LambdaQueryWrapper<SetmealDish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.in(SetmealDish::getSetmealId, ids);
        setMealDishService.remove(dishLambdaQueryWrapper);
        return Result.success("删除成功！");
    }

    /**
     * 根据id（批量）起售/停售套餐
     *
     * @param status
     * @param ids
     * @return
     */
    @Override
    public Result<String> updateSetMealStatus(Integer status, String ids) {
        String[] idArr = ids.split(",");
        Arrays.stream(idArr).forEach((id) -> {
            Setmeal setmeal = setMealMapper.selectById(id);
            setmeal.setStatus(status);
            setMealMapper.updateById(setmeal);
        });
        return Result.success("修改成功！");
    }

    /**
     * 移动端套餐展示
     *
     * @param setmeal
     * @return
     */
    @Cacheable(value = "SetMealCache", key = "#setmeal.categoryId+'_'+#setmeal.status")
    @Override
    public Result<List<Setmeal>> getSetMealList(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null, Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null, Setmeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> setMealList = setMealMapper.selectList(queryWrapper);
        return Result.success(setMealList);
    }

    /**
     * 点击套餐图片展示所含菜品
     *
     * @param id
     * @return
     */
    @Override
    public Result<List<DishDto>> getDishListBySetMeal(Long id) {
        LambdaQueryWrapper<SetmealDish> setMealDishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setMealDishLambdaQueryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> setMealDishList = setMealDishService.list(setMealDishLambdaQueryWrapper);
        List<DishDto> dishDtoList = setMealDishList.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            Dish dish = dishService.getById(item.getDishId());
            BeanUtils.copyProperties(dish, dishDto);
            dishDto.setCopies(item.getCopies());
            return dishDto;
        }).collect(Collectors.toList());
        return Result.success(dishDtoList);
    }
}