package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

    @Select("select * from employee where username = #{username} and is_deleted = 1")
    Employee findByUsername(String username);

    @Update("update employee set name = #{name}, password = #{password}, phone = #{phone}, " +
            "sex = #{sex}, id_number = #{idNumber}, status = #{status}, create_time = #{createTime}, " +
            "update_time = #{updateTime}, create_user = #{createUser}, update_user = #{updateUser}, " +
            "is_deleted = 0 where username = #{username}")
    void updateEmp(Employee e);

}
