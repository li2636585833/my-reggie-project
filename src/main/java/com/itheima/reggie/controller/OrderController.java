package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 订单明细列表分页查询
     *
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("/page")
    public Result<Page<OrderDto>> getOrderListByPage(
            int page, int pageSize, String number,
            @DateTimeFormat(fallbackPatterns = "yyyy-MM-dd HH:mm:ss") LocalDateTime beginTime,
            @DateTimeFormat(fallbackPatterns = "yyyy-MM-dd HH:mm:ss") LocalDateTime endTime) {
        log.info("前台传来的page：{},pageSize:{},number:{},beginTime:{},endTime:{}", page, pageSize, number, beginTime, endTime);
        return orderService.getOrderListByPage(page, pageSize, number, beginTime, endTime);
    }

    /**
     * 立即支付
     *
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public Result<String> orderPay(@RequestBody Orders orders) {
        return orderService.orderPay(orders);
    }

    /**
     * 移动端用户订单查看
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public Result<Page<OrderDto>> userPage(int page, int pageSize) {
        return orderService.userPage(page, pageSize);
    }

    /**
     * 修改订单状态
     *
     * @param orders
     * @return
     */
    @PutMapping
    public Result<String> updateOrderStatusTo3(@RequestBody Orders orders) {
        return orderService.updateOrderStatusTo3(orders);
    }

    /**
     * 再来一单
     *
     * @param id
     * @return
     */
    @PostMapping("/again")
    public Result<String> orderAgain(@RequestBody Orders orders) {
        log.info("前台传来的id为：{}", orders);
        return orderService.orderAgain(orders);
    }
}
