package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.mapper.CategoryMapper;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.mapper.SetMealMapper;
import com.itheima.reggie.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetMealMapper setMealMapper;

    /**
     * 菜品分类列表分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Result<Page<Category>> getCategoryListByPage(int page, int pageSize) {
        Page<Category> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByAsc(Category::getSort);
        categoryMapper.selectPage(pageInfo, lambdaQueryWrapper);
        return Result.success(pageInfo);
    }

    /**
     * 添加分类
     *
     * @param category
     * @return
     */
    @Override
    public Result<String> addCategory(Category category) {
        categoryMapper.insert(category);
        return Result.success("添加成功！");
    }

    /**
     * 删除分类
     *
     * @param ids
     * @return
     */
    @Override
    public Result<String> deleteCategoryById(List<Long> ids) {
        for (Long id : ids) {
            //判断当前要删除的分类中有没有菜
            LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
            dishLambdaQueryWrapper.eq(Dish::getCategoryId, id);
            List<Dish> dishList = dishMapper.selectList(dishLambdaQueryWrapper);
            if (dishList != null && dishList.size() > 0) {
                throw new CustomException("删除失败，当前分类有关联菜品！");
            }

            //判断它属不属于哪个套餐
            LambdaQueryWrapper<Setmeal> setMealLambdaQueryWrapper = new LambdaQueryWrapper<>();
            setMealLambdaQueryWrapper.eq(Setmeal::getCategoryId, id);
            List<Setmeal> setMealList = setMealMapper.selectList(setMealLambdaQueryWrapper);
            if (setMealList != null && setMealList.size() > 0) {
                throw new CustomException("删除失败，当前套餐有关联菜品！");
            }
            categoryMapper.deleteById(id);
        }

        return Result.success("删除成功！");
    }

    /**
     * 修改分类
     *
     * @param category
     * @return
     */
    @Override
    public Result<String> updateCategory(Category category) {
        categoryMapper.updateById(category);
        return Result.success("修改成功！");
    }

    /**
     * 获取分类列表
     *
     * @param category
     * @return
     */
    @Override
    public Result<List<Category>> getCategoryListByType(Category category) {
        LambdaQueryWrapper<Category> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(category.getType() != null, Category::getType, category.getType());
        lambdaQueryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);
        List<Category> categoryList = categoryMapper.selectList(lambdaQueryWrapper);
        return Result.success(categoryList);
    }
}
