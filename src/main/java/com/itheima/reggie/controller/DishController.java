package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    /**
     * 菜品列表分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public Result<Page<DishDto>> getDishListByPage(int page, int pageSize, String name) {
        return dishService.getDishListByPage(page, pageSize, name);
    }

    /**
     * 添加菜品
     *
     * @param dishDto
     * @return
     */
    @PostMapping
    public Result<String> addDish(@RequestBody DishDto dishDto) {
        return dishService.addDish(dishDto);
    }

    /**
     * 根据id查询菜品信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<DishDto> getDishDtoById(@PathVariable Long id) {
        return dishService.getDishDtoById(id);
    }

    /**
     * 修改菜品
     *
     * @param dishDto
     * @return
     */
    @PutMapping
    public Result<String> updateDishDto(@RequestBody DishDto dishDto) {
        return dishService.updateDishDto(dishDto);
    }

    /**
     * 根据id修改状态
     *
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public Result<String> updateDishStatus(@PathVariable Integer status, String ids) {
        log.info("前台传来的状态为：{}", status);
        log.info("前台传来的id为：{}", ids);
        return dishService.updateDishStatus(status, ids);
    }

    /**
     * 根据id删除菜品
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    public Result<String> deleteDish(@RequestParam List<Long> ids) {
        log.info("前台传来的要删除的菜品id为：{}", ids);
        return dishService.deleteDish(ids);
    }

    /**
     * 根据分类id查询菜品
     *
     * @param dish
     * @return
     */
    @GetMapping("/list")
    public Result<List<DishDto>> getDishListByCategoryId(Dish dish) {
        log.info("前台传来的categoryId为：{}", dish);
        return dishService.getDishListByCategoryId(dish);
    }
}
