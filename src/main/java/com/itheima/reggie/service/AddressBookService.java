package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.AddressBook;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface AddressBookService extends IService<AddressBook> {
    Result<AddressBook> addAddressBook(AddressBook addressBook);

    Result<AddressBook> setDefault(AddressBook addressBook);

    Result<AddressBook> getAddressBookById(Long id);

    Result<AddressBook> getDefaultAddressBook();

    Result<List<AddressBook>> getAddressBookList(AddressBook addressBook);

    Result<String> updateAddressBook(AddressBook addressBook);

    Result<String> deleteAddressBookById(Long id);
}
