package com.itheima.reggie.controller;

import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.UserDto;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 发送短信验证码
     *
     * @param request
     * @param user
     * @return
     */
    @PostMapping("/sendMsg")
    public Result<String> sendMsgToUser(HttpServletRequest request, @RequestBody User user) {
        log.info("前台传来的user为：{}", user);
        return userService.sendMsgToUser(request, user);
    }

    /**
     * 用户登录
     *
     * @return
     */
    @PostMapping("/login")
    public Result<User> userLogin(HttpServletRequest request, @RequestBody UserDto userDto) {
        log.info("前台传的phone:{},code:{}", userDto.getPhone(), userDto.getCode());
        return userService.userLogin(request, userDto);
    }

    /**
     * 用户退出登录
     *
     * @return
     */
    @PostMapping("/loginout")
    public Result<String> logout() {
        //BaseContext.removeCurrentId();
        return Result.success("退出成功！");
    }
}
