package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface DishService extends IService<Dish> {
    Result<Page<DishDto>> getDishListByPage(int page, int pageSize, String name);

    Result<String> addDish(DishDto dishDto);

    Result<DishDto> getDishDtoById(Long id);

    Result<String> updateDishDto(DishDto dishDto);

    Result<String> updateDishStatus(Integer status, String ids);

    Result<String> deleteDish(List<Long> ids);

    Result<List<DishDto>> getDishListByCategoryId(Dish dish);
}
