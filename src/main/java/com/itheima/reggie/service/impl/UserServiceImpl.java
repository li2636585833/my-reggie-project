package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.UserDto;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.UserService;
import com.itheima.reggie.util.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Result<String> sendMsgToUser(HttpServletRequest request, User user) {
        String phone = user.getPhone();
        if (!StringUtils.isNotEmpty(phone)) {
            return Result.error("短信发送失败！");
        }
        String code = ValidateCodeUtils.generateValidateCode4String(4);
        log.info("生成的验证码code为：{}", code);

        //SMSUtils.sendMessage("瑞吉外卖", "SMS_462205198", phone, code);

        //将生成的验证码缓存到redis中，设置有效期为五分钟
        redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.MINUTES);

        //request.getSession().setAttribute(phone, code);
        return Result.success("短信发送成功，请查看！");
    }

    @Override
    public Result<User> userLogin(HttpServletRequest request, UserDto userDto) {
        String phone = userDto.getPhone();
        String code = userDto.getCode();
        //String codeInSession = (String) request.getSession().getAttribute(phone);

        //从redis中获取缓存验证码
        Object codeInRedis = redisTemplate.opsForValue().get(phone);

        if (codeInRedis != null && codeInRedis.equals(code)) {
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone, phone);
            User user = userMapper.selectOne(queryWrapper);
            if (user == null) {
                user = new User();
                user.setPhone(phone);
                userMapper.insert(user);
            }
            request.getSession().setAttribute("user", user);

            //登录成功，删除redis中缓存验证码
            redisTemplate.delete(phone);

            return Result.success(user);
        }
        return Result.error("验证码错误！");
    }
}
