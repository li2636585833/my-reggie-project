package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetMealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.service.SetMealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetMealController {

    @Autowired
    private SetMealService setMealService;

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @PostMapping
    public Result<String> addSetMeal(@RequestBody SetMealDto setmealDto) {
        return setMealService.addSetMeal(setmealDto);
    }

    /**
     * 套餐列表分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public Result<Page<SetMealDto>> getSetMealDtoList(int page, int pageSize, String name) {
        return setMealService.getSetMealDtoList(page, pageSize, name);
    }

    /**
     * 根据id查询套餐详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<SetMealDto> getSetMealById(@PathVariable Long id) {
        log.info("前台传来的套餐id为：{}", id);
        return setMealService.getSetMealById(id);
    }

    /**
     * 修改套餐
     *
     * @param setMealDto
     * @return
     */
    @PutMapping
    public Result<String> updateSetMeal(@RequestBody SetMealDto setMealDto) {
        log.info("前台传来的修改套餐内容为：{}", setMealDto);
        return setMealService.updateSetMeal(setMealDto);
    }

    /**
     * 删除套餐
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    public Result<String> deleteSetMeal(@RequestParam List<Long> ids) {
        log.info("前台传来的要删除的套餐id为：{}", ids);
        return setMealService.deleteSetMeal(ids);
    }

    /**
     * 根据id（批量）起售/停售套餐
     *
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public Result<String> updateSetMealStatus(@PathVariable Integer status, String ids) {
        log.info("前台传来的status：{}，ids：{}", status, ids);
        return setMealService.updateSetMealStatus(status, ids);
    }

    /**
     * 移动端套餐展示
     *
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    public Result<List<Setmeal>> getSetMealList(Setmeal setmeal) {
        return setMealService.getSetMealList(setmeal);
    }

    /**
     * 点击套餐图片展示所含菜品
     *
     * @param id
     * @return
     */
    @GetMapping("/dish/{id}")
    public Result<List<DishDto>> getDishListBySetMeal(@PathVariable Long id) {
        log.info("前台传来的id：{}", id);
        return setMealService.getDishListBySetMeal(id);
    }
}
