package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.Orders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Transactional
public interface OrderService extends IService<Orders> {
    Result<Page<OrderDto>> getOrderListByPage(int page, int pageSize, String number, LocalDateTime beginTime, LocalDateTime endTime);

    Result<String> orderPay(Orders orders);

    Result<Page<OrderDto>> userPage(int page, int pageSize);

    Result<String> updateOrderStatusTo3(Orders orders);

    Result<String> orderAgain(Orders orders);
}
