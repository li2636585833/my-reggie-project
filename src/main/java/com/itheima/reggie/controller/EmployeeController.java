package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     *
     * @param request
     * @param employee
     * @return
     */
    @PostMapping("/login")
    public Result<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {
        return employeeService.login(request, employee);
    }

    /**
     * 员工退出
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    public Result<String> logout(HttpServletRequest request) {
        return employeeService.logout(request);
    }

    /**
     * 添加员工
     *
     * @param employee
     * @return
     */
    @PostMapping
    public Result<String> addEmp(HttpServletRequest request, @RequestBody Employee employee) {
        return employeeService.addEmp(request, employee);
    }

    /**
     * 用户列表分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public Result<Page<Employee>> page(int page, int pageSize, String name) {
        return employeeService.getEmpListByPage(page, pageSize, name);
    }

    /**
     * 根据id修改员工状态
     *
     * @param employee
     * @return
     */
    @PutMapping
    public Result<String> updateEmp(HttpServletRequest request, @RequestBody Employee employee) {
        return employeeService.updateEmp(request, employee);
    }

    /**
     * 根据id查询员工
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Employee> getEmpById(@PathVariable Long id) {
        return employeeService.getEmpById(id);
    }

    /**
     * 删除员工
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<String> deleteEmpById(@PathVariable Long id) {
        log.info("要删除员工的id为：{}", id);
        return employeeService.deleteEmpById(id);
    }
}
