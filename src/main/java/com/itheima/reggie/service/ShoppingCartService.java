package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.ShoppingCart;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ShoppingCartService extends IService<ShoppingCart> {
    Result<ShoppingCart> addShoppingCart(ShoppingCart shoppingCart);

    Result<List<ShoppingCart>> getShoppingCartList();

    Result<String> deleteShoppingCart();

    Result<String> subNumber(ShoppingCart shoppingCart);
}
