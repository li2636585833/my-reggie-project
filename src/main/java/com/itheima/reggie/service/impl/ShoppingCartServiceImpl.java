package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.mapper.ShoppingCartMapper;
import com.itheima.reggie.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * 添加到购物车
     *
     * @param shoppingCart
     * @return
     */
    @Override
    public Result<ShoppingCart> addShoppingCart(ShoppingCart shoppingCart) {
        //首先获取当前登录人的id
        Long userId = BaseContext.getCurrentId();
        shoppingCart.setUserId(userId);
        //判断当前插入的菜品或者套餐是否存在
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        if (shoppingCart.getDishId() != null) {
            //添加菜品
            queryWrapper.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
        } else {
            //添加套餐
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        ShoppingCart shoppingCart1 = shoppingCartMapper.selectOne(queryWrapper);
        if (shoppingCart1 != null) {
            //如果存在，则number+1
            Integer number = shoppingCart1.getNumber();
            number += 1;
            shoppingCart1.setNumber(number);
            shoppingCartMapper.updateById(shoppingCart1);
        } else {
            //如果不存在，则直接插入
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartMapper.insert(shoppingCart);
            shoppingCart1 = shoppingCart;
        }
        return Result.success(shoppingCart1);
    }

    /**
     * 查询购物车列表
     *
     * @return
     */
    @Override
    public Result<List<ShoppingCart>> getShoppingCartList() {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        queryWrapper.orderByAsc(ShoppingCart::getCreateTime);
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectList(queryWrapper);
        return Result.success(shoppingCartList);
    }

    /**
     * 删除购物车信息
     *
     * @return
     */
    @Override
    public Result<String> deleteShoppingCart() {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        shoppingCartMapper.delete(queryWrapper);
        return Result.success("删除成功！");
    }

    /**
     * 减少商品
     *
     * @param shoppingCart
     * @return
     */
    @Override
    public Result<String> subNumber(ShoppingCart shoppingCart) {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        if (shoppingCart.getDishId() != null) {
            queryWrapper.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
        }else {
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        shoppingCartMapper.delete(queryWrapper);
        return Result.success("减少成功！");
    }
}
