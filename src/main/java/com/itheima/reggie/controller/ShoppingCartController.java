package com.itheima.reggie.controller;

import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加到购物车
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public Result<ShoppingCart> addShoppingCart(@RequestBody ShoppingCart shoppingCart) {
        log.info("移动端传来的添加到购物车的内容为：{}", shoppingCart);
        return shoppingCartService.addShoppingCart(shoppingCart);
    }

    /**
     * 查询购物车列表
     *
     * @return
     */
    @GetMapping("/list")
    public Result<List<ShoppingCart>> getShoppingCartList() {
        return shoppingCartService.getShoppingCartList();
    }

    /**
     * 删除购物车信息
     *
     * @return
     */
    @DeleteMapping("/clean")
    public Result<String> deleteShoppingCart() {
        return shoppingCartService.deleteShoppingCart();
    }

    /**
     * 减少商品
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public Result<String> subNumber(@RequestBody ShoppingCart shoppingCart) {
        log.info("前台传来的shoppingCart：{}", shoppingCart);
        return shoppingCartService.subNumber(shoppingCart);
    }

}
