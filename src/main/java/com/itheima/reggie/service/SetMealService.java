package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetMealDto;
import com.itheima.reggie.entity.Setmeal;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface SetMealService extends IService<Setmeal> {
    Result<String> addSetMeal(SetMealDto setmealDto);

    Result<Page<SetMealDto>> getSetMealDtoList(int page, int pageSize, String name);

    Result<SetMealDto> getSetMealById(Long id);

    Result<String> updateSetMeal(SetMealDto setMealDto);

    Result<String> deleteSetMeal(List<Long> ids);

    Result<String> updateSetMealStatus(Integer status, String ids);

    Result<List<Setmeal>> getSetMealList(Setmeal setmeal);

    Result<List<DishDto>> getDishListBySetMeal(Long id);
}
