package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 菜品列表分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Result<Page<DishDto>> getDishListByPage(int page, int pageSize, String name) {
        //创建一个Dish分页对象，传入page和pageSize参数
        Page<Dish> dishPage = new Page<>(page, pageSize);
        //创建一个DishDto分页对象，目的是为了页面中categoryName内容的显示
        Page<DishDto> dishDtoPage = new Page<>();
        //根据name模糊查，根据sort降序排列
        LambdaQueryWrapper<Dish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(name != null, Dish::getName, name);
        lambdaQueryWrapper.orderByDesc(Dish::getSort);
        dishMapper.selectPage(dishPage, lambdaQueryWrapper);
        //将查到的dishPage中的内容拷贝到dishDtoPage中，但是不拷贝records，因为其中没有categoryName，只有categoryId
        BeanUtils.copyProperties(dishPage, dishDtoPage, "records");
        //下面对dishPage中的records数据进行处理
        //首先拿到dishPage中的records集合dishList
        List<Dish> dishList = dishPage.getRecords();
        //通过stream流的方式将Dish类型的dishList转换为DishDtoList类型
        List<DishDto> dishDtoList = dishList.stream().map((item) -> {
            //创建一个DishDto对象，目的是给其中的categoryName赋值
            DishDto dishDto = new DishDto();
            //拷贝dishList其中每个item的普通属性，不包含categoryId
            BeanUtils.copyProperties(item, dishDto);
            //下面通过getCategoryId方法拿到每个item的categoryId
            Long categoryId = item.getCategoryId();
            //然后找到category表中每个item的categoryId对应的categoryName的值
            Category category = categoryService.getById(categoryId);
            String categoryName = category.getName();
            //将其赋值给dishDto对象的categoryName属性
            dishDto.setCategoryName(categoryName);
            //返回每一个dishDto对象
            return dishDto;
            //下面将所有对象收集起来转换为DishDto类型的list集合
        }).collect(Collectors.toList());
        //将新生成的dishDtoList赋值给dishDtoPage的records属性，其中就包含页面要展示的categoryName的内容
        dishDtoPage.setRecords(dishDtoList);
        //返回给controller
        return Result.success(dishDtoPage);
    }

    /**
     * 添加菜品
     *
     * @param dishDto
     * @return
     */
    @Override
    public Result<String> addDish(DishDto dishDto) {

        //将redis中的dish_开头的所有数据全部清理
        //Set keys = redisTemplate.keys("dish_*");
        //redisTemplate.delete(keys);

        //精确清理：清理某个分类下面的菜品缓存
        String key = "dish_" + dishDto.getCategoryId() + "_status_1";
        redisTemplate.delete(key);

        this.save(dishDto);

        Long dishId = dishDto.getId();
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());
        dishFlavorService.saveBatch(flavors);

        return Result.success("新增成功！");
    }

    /**
     * 根据id查询dishDto
     *
     * @param id
     * @return
     */
    @Override
    public Result<DishDto> getDishDtoById(Long id) {
        Dish dish = dishMapper.selectById(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish, dishDto);
        LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId, dish.getId());
        List<DishFlavor> dishFlavorList = dishFlavorService.list(dishFlavorLambdaQueryWrapper);
        dishDto.setFlavors(dishFlavorList);
        return Result.success(dishDto);
    }

    /**
     * 修改菜品
     *
     * @param dishDto
     * @return
     */
    @Override
    public Result<String> updateDishDto(DishDto dishDto) {

        //将redis中的dish_开头的所有数据全部清理
        //Set keys = redisTemplate.keys("dish_*");
        //redisTemplate.delete(keys);

        //精确清理：清理某个分类下面的菜品缓存
        String key = "dish_" + dishDto.getCategoryId() + "_status_1";
        redisTemplate.delete(key);

        LambdaQueryWrapper<Dish> dishQueryWrapper = new LambdaQueryWrapper<>();
        dishQueryWrapper.eq(Dish::getName, dishDto.getName());
        List<Dish> dishList = dishMapper.selectList(dishQueryWrapper);
        if (dishList != null && dishList.size() > 0) {
            for (Dish dish : dishList) {
                if (!dish.getId().equals(dishDto.getId())) {
                    return Result.error("该菜品已存在！");
                }
            }
        }
        this.updateById(dishDto);
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dishDto.getId());
        dishFlavorService.remove(queryWrapper);

        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(flavors);
        return Result.success("修改成功！");
    }

    /**
     * 根据id修改状态
     *
     * @param status
     * @param ids
     * @return
     */
    @Override
    public Result<String> updateDishStatus(Integer status, String ids) {
        String[] idArr = ids.split(",");

        Arrays.stream(idArr).forEach((id) -> {
            Dish dish = dishMapper.selectById(id);
            dish.setStatus(status);
            dishMapper.updateById(dish);
        });
        return Result.success("修改成功！");
    }

    /**
     * 根据id删除菜品
     *
     * @param ids
     * @return
     */
    @Override
    public Result<String> deleteDish(List<Long> ids) {
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.in(Dish::getId, ids);
        dishLambdaQueryWrapper.eq(Dish::getStatus, 1);
        int count = this.count(dishLambdaQueryWrapper);
        if (count > 0) {
            return Result.error("菜品正在售卖，不能删除！");
        }

        this.removeByIds(ids);

        LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishFlavorLambdaQueryWrapper.in(DishFlavor::getDishId, ids);
        dishFlavorService.remove(dishFlavorLambdaQueryWrapper);
        return Result.success("删除成功！");
    }

    /**
     * 根据分类id查询菜品
     *
     * @param dish
     * @return
     */
    @Override
    public Result<List<DishDto>> getDishListByCategoryId(Dish dish) {
        List<DishDto> dishDtoList = null;
        //构造key
        //dish_56465165486_status_1
        String key = "dish_" + dish.getCategoryId() + "_status_" + dish.getStatus();

        //从redis中获取缓存数据
        dishDtoList = (List<DishDto>) redisTemplate.opsForValue().get(key);
        //如果存在，则直接返回
        if (dishDtoList != null && dishDtoList.size() > 0) {
            return Result.success(dishDtoList);
        }
        //如果不存在，则先从数据库中查询
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //queryWrapper.eq(Dish::getStatus, dish.getStatus());
        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId, dish.getCategoryId());
        queryWrapper.eq(dish.getStatus() != null, Dish::getStatus, dish.getStatus());
        List<Dish> dishList = dishMapper.selectList(queryWrapper);
        dishDtoList = dishList.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);
            Long categoryId = item.getCategoryId();
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }

            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> flavorLambdaQueryWrapper = new LambdaQueryWrapper<>();
            flavorLambdaQueryWrapper.eq(DishFlavor::getDishId, dishId);
            List<DishFlavor> dishFlavorList = dishFlavorService.list(flavorLambdaQueryWrapper);
            dishDto.setFlavors(dishFlavorList);
            return dishDto;
        }).collect(Collectors.toList());

        //然后将数据库的数据缓存到redis
        redisTemplate.opsForValue().set(key, dishDtoList, 60, TimeUnit.MINUTES);

        return Result.success(dishDtoList);
    }
}