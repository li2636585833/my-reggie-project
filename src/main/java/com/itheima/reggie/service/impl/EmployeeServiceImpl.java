package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.mapper.EmployeeMapper;
import com.itheima.reggie.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param request
     * @param employee
     * @return
     */
    @Override
    public Result<Employee> login(HttpServletRequest request, Employee employee) {
        //1、将页面提交的密码password进行md5加密处理
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        //2、根据页面提交的用户名username查询数据库
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername, employee.getUsername());
        Employee emp = employeeMapper.selectOne(queryWrapper);
        if (emp == null) {
            //3、如果没有查询到则返回登录失败结果
            return Result.error("用户不存在！");
        }
        //4、密码比对，如果不一致则返回登录失败结果
        if (!password.equals(emp.getPassword())) {
            return Result.error("密码输入错误！");
        }
        //5、查看员工状态，如果为已禁用状态，则返回员工已禁用结果
        if (emp.getStatus() == 0) {
            return Result.error("该员工已禁用！");
        }
        //6、登录成功，将员工id存入Session并返回登录成功结果
        request.getSession().setAttribute("emp", emp);
        return Result.success(emp);
    }

    /**
     * 员工退出登录
     *
     * @param request
     * @return
     */
    @Override
    public Result<String> logout(HttpServletRequest request) {
        //1、清理Session中的用户id
        request.getSession().removeAttribute("emp");
        //2、返回结果
        return Result.success("退出成功！");
    }

    /**
     * 添加员工
     *
     * @param request
     * @param employee
     * @return
     */
    @Override
    public Result<String> addEmp(HttpServletRequest request, Employee employee) {

        Employee e = employeeMapper.findByUsername(employee.getUsername());
        if (e != null) {
            if (employee.getIdNumber().equals(e.getIdNumber())) {
                e.setName(employee.getName());
                e.setPhone(employee.getPhone());
                e.setSex(employee.getSex());
                employeeMapper.updateEmp(e);
            } else {
                //身份证号不一样，不是一个人，继续添加
                e.setId(null);
                e.setName(employee.getName());
                e.setPhone(employee.getPhone());
                e.setSex(employee.getSex());
                e.setIdNumber(employee.getIdNumber());
                String username = employee.getUsername();
                long now = System.currentTimeMillis();
                e.setUsername(username + now);
                e.setIsDeleted(0);
                employeeMapper.insert(e);
            }
            return Result.success("添加成功！");
        }

        //1.设置默认密码123，需要先进行MD5加密
        employee.setPassword(DigestUtils.md5DigestAsHex("123".getBytes()));
        //2.将employee对象添加到数据库
        employeeMapper.insert(employee);
        return Result.success("添加成功！");
    }

    /**
     * 员工列表分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Result<Page<Employee>> getEmpListByPage(int page, int pageSize, String name) {
        Page<Employee> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Employee> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByDesc(Employee::getUpdateTime);
        lambdaQueryWrapper.like(name != null, Employee::getName, name);
        employeeMapper.selectPage(pageInfo, lambdaQueryWrapper);
        return Result.success(pageInfo);
    }

    /**
     * 根据id修改员工状态
     *
     * @param employee
     * @return
     */
    @Override
    public Result<String> updateEmp(HttpServletRequest request, Employee employee) {
        employeeMapper.updateById(employee);
        return Result.success("修改成功！");
    }

    /**
     * 根据id查询员工
     *
     * @param id
     * @return
     */
    @Override
    public Result<Employee> getEmpById(Long id) {
        Employee employee = employeeMapper.selectById(id);
        return Result.success(employee);
    }

    /**
     * 删除员工
     *
     * @param id
     * @return
     */
    @Override
    public Result<String> deleteEmpById(Long id) {
        employeeMapper.deleteById(id);
        return Result.success("删除成功！");
    }
}
