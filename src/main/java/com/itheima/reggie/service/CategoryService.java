package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.common.Result;
import com.itheima.reggie.entity.Category;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface CategoryService extends IService<Category> {
    Result<Page<Category>> getCategoryListByPage(int page, int pageSize);

    Result<String> addCategory(Category category);

    Result<String> deleteCategoryById(List<Long> ids);

    Result<String> updateCategory(Category category);

    Result<List<Category>> getCategoryListByType(Category category);
}
